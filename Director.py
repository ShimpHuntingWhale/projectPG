# pylint: disable=E1101

import pygame

from GameScene  import GameScene
from TitleScene import TitleScene
from enum       import IntEnum

class Director:
    class SCENE_INDEX(IntEnum):
        TITLE, GAME = range(2)

    # pygame initalize
    def __init__(self, width, height):
        pygame.init()
        self.size = (width, height)
        self.screen = pygame.display.set_mode(self.size)
        self.sprite_screen = pygame.display.set_mode(self.size)
        self.clock = pygame.time.Clock()
        pygame.display.set_caption("Framework Project")

        self.isLoop = True
    
        self.sceneIndex = self.SCENE_INDEX.TITLE
        self.scenes = [
            TitleScene(), GameScene()
        ]
        self.sc = self.scenes[self.sceneIndex]
    
    # key event
    def keyevent(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.isLoop = False
            else:
                index = self.sc.keyevent(event)
                if index is not None:
                    self.changeScene(index)
    
    # update
    def update(self):
        self.keyevent()
        
        index = self.sc.update()
        if index is not None:
            self.changeScene(index)

    # render
    def render(self):
        # 화면 클리어
        self.screen.fill((255, 255, 255))

        # 씬 그리기
        self.sc.render(self.screen)

        # 화면 전환
        pygame.display.flip()

    # Change Scene
    def changeScene(self, index):
        if index >= 0 and index < len(self.scenes):
            self.sceneIndex = index
            self.sc = self.scenes[index]
            self.sc.init()

    # Main Loop
    def loop(self):
        while self.isLoop:
            self.update()
            self.render()

            # 60fps
            self.clock.tick(60)
        else:
            pygame.quit()