# pylint: disable=E1101

import pygame

from DefaultScene import DefaultScene
from Sprite import Sprite
from SpriteSheet import SpriteSheet
from Camera import Camera

class TitleScene(DefaultScene):
    def __init__(self):
        DefaultScene.__init__(self)
        #self.bgImg = Sprite('./img/sprite/title/bg.png')

        #self.selectImg = SpriteSheet('./img/sprite/title/select.png', './img/sprite/title/select.json')
        #self.selectImg.setAnchorPos([0.5, 0.5])
        #self.selectImg.setPos([400, 400])

        self.ss = SpriteSheet('./img/sprite/items.png', './img/sprite/items.json')

        self.init()
    
    def init(self):
        #self.selectImg.init()
        self.ss.init()
        pass

    def keyevent(self, event):
        #if event.type == pygame.KEYDOWN:
        #    if event.key == pygame.K_DOWN:
        #        self.selectImg.increaseIndex()

        #    if event.key == pygame.K_UP:
        #        self.selectImg.decreaseIndex()
        #    
        #    # Enter
        #    if event.key == 13:
        #        return 1
        
        return None

    def update(self):
        self.ss.increaseIndex()
        return None
    
    def render(self, screen):
        #self.bgImg.render(screen, self.camera)
        #self.selectImg.render(screen, self.camera)
        self.ss.render(screen, self.camera)