
import json
import pygame
from Image  import Image
from Sprite import Sprite
from check_value import check_list

from pprint import pprint

class SpriteSheet():
    def __init__(self, img_path, info_path):
        self.original = Image(img_path)
        with open(info_path, 'r', encoding='UTF8') as f:
            self.info = json.load(f)

        self.sub_surfaces = []
        for i in self.info['frames']:
            _x, _y, _width, _height = i['frame']['x'], i['frame']['y'], i['frame']['w'], i['frame']['h']
            self.sub_surfaces.append(
                Sprite(self.original.img.subsurface((_x, _y, _width, _height)))
            )
        self.index = 0
    
    def init(self):
        self.setIndex(0)

    def setIndex(self, index):
        self.index = index
    
    def increaseIndex(self):
        self.index += 1
        if self.index > len(self.sub_surfaces) - 1:
            self.index = 0
    
    def decreaseIndex(self):
        self.index -= 1
        if self.index < 0:
            self.index = len(self.sub_surfaces) - 1

    def setPos(self, pos):
        for i in self.sub_surfaces:
            i.setPos(pos)
    
    def setAnchorPos(self, pos):
        for i in self.sub_surfaces:
            i.setAnchorPos(pos)

    def render(self, screen, camera):
        self.sub_surfaces[self.index].render(screen, camera)