import pygame

class Font:
    def __init__(self, fontname, size, color):
        self.fontname = fontname
        self.size, self.color = size, color
    
    def render(self, screen, text, pos):
        sf = pygame.font.SysFont(self.fontname, self.size)
        _text_ = sf.render(text, True, self.color)
        screen.blit(_text_, pos)