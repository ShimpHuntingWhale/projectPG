
from check_value import check_list, check_int

class Position:
    def __init__(self, pos):
        self.x, self.y = 0, 0
        self.setPosition(pos)

    def getPosition(self):
        return [self.x, self.y]
    
    def getPositionByTuple(self):
        return (self.x, self.y)
    
    def setPosition(self, pos):
        if check_list(pos) is not None:
            self.x = pos[0]
            self.y = pos[1]

    def setPositionX(self, x):
        if check_int(x) is not None:
            self.x = x
    
    def setPositionY(self, y):
        if check_int(y) is not None:
            self.y = y