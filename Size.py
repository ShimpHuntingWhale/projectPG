
from check_value import check_list, check_int

class Size:
    def __init__(self, size):
        self.width, self.height = 0, 0
        self.setSize(size)

    def setSize(self, size):
        if check_list(size) is not None:
            self.setWidth(size[0])
            self.setHeight(size[1])
    
    def setWidth(self, w):
        if check_int(w) is not None:
            self.width = w
    
    def setHeight(self, h):
        if check_int(h) is not None:
            self.height = h

    def getSizeByList(self):
        return [self.width, self.height]
    
    def getSizeByTuple(self):
        return (self.width, self.height)