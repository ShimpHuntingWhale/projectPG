
def check_type(value, user_type):
    if type(value) is user_type:
        return value
    else:
        print('value is not {}'.format(user_type))
        return None

def check_list(value):
    return check_type(value, list)

def check_tuple(value):
    return check_type(value, tuple)

def check_str(value):
    return check_type(value, str)

def check_int(value):
    return check_type(value, int)