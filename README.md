# Framework for pygame

## Using Tools:
* python 3.6
    * pygame
    * py2exe
    * pyinstaller
* vscode
    * dracula official
    * python extension
    * korean extension
* git/gitlab

## 참고사이트
* [뇌를 자극하는 파이썬 3 - Pygame으로 게임 만들기](https://yoonkh.github.io/python/2017/12/10/brain7.html)
* https://nerdparadise.com/programming/pygameblitopacity
* https://www.pygame.org/docs/ref/surface.html

## Date:
* 2018-08-05:
    * Start to write framework
    * Add Class
        * Image
            * setFlip()
            * setPos()
        * Sprite: Image
            * collisionBox
            * setCollisionBox
        * Player: Sprite
        * Animation
        * Animation Director
        * DefaultScene
        * GameScene: DefaultScene
        * Font
        * MainLoop -> Director: Changed Name
* 2018-08-06:
    * Retouch Class:
        * Animation: 
            * once/infinity mode
        * Sprite:
            * setOpacity()
    * Add Class:
        * Audio
* 2018-08-07:
    * Retouch Class:
        * Image:
            * setScale()
        * Player:
            * Add Animations
    * Add Class:
        * Camera
* 2018-08-08:
    * Add Module:
        * check_value.py
            * check_list
            * check_tuple
    * Add Class:
        * Camera
    * Retouch Class:
        * Sprite
            * render
* 2018-08-09:
    * Retouch Class:
        * Camera
* 2018-08-10:
    * install py2exe / pyinstaller
        * pyinstaller.exe -F --noconsole main.py
* 2018-08-12:
    * Add Class:
        * Title Scene
    * Retouch Class:
        * Retouch for change scene
    * Add Module:
        * check_value.py
            * check_type
* 2018-08-13:
    * Add Class:
        * SpriteSheet
    * Retouch Class:
        * Image
* 2018-08-18:
    * Add Class:
        * Position
        * Rect
        * Size