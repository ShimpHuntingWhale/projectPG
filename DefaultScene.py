
from abc import abstractmethod, ABCMeta
from Size   import Size
from Camera import Camera

class DefaultScene:
    __metaclass__ = ABCMeta

    @abstractmethod
    def __init__(self):
        self.winSize = Size([800, 600])
        self.camera = Camera(self.winSize, self.winSize, None)

    @abstractmethod
    def init(self):
        pass

    @abstractmethod
    def keyevent(self, event):
        return None
    
    @abstractmethod
    def update(self, screen):
        return None

    @abstractmethod
    def render(self, screen):
        pass