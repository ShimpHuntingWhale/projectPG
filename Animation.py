
from datetime import datetime
from Image import Image

def get_epochtime_ms():
    return round(datetime.utcnow().timestamp() * 1000)

class Animation:
    def __init__(self, files, ms, nextAnimationIndex = -1):
        self.imgs = [Image(i) for i in files]
        
        self.index = 0
        self.maxIndex = len(self.imgs)
        self.ms = ms
        self.startT = get_epochtime_ms()
        self.isStop = False
        
        self.nextIndex = nextAnimationIndex
        self.mode = "infinity"
        if nextAnimationIndex != -1:
            self.mode = "once"
            
    def changeImg(self, sprite):
        if self.index < self.maxIndex - 1:
            self.index += 1
        else:
            self.index = 0
            if self.mode == "once":
                return self.nextIndex
        # Flip
        self.imgs[self.index].setFlip(sprite.isFlip[0], sprite.isFlip[1])

        # Scale
        self.imgs[self.index].setScale(sprite.scale[0], sprite.scale[1])

        # ChangeImg
        sprite.changeImg(self.imgs[self.index])

        return -1

    def play(self):
        self.isStop = False
    def stop(self):
        self.isStop = True
    
    def update(self, sprite):
        if self.isStop:
            self.startT = get_epochtime_ms()
        else:
            nowT = get_epochtime_ms()
            if nowT - self.startT > self.ms:
                self.startT = get_epochtime_ms()
                return self.changeImg(sprite)
        return -1