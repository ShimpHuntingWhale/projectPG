
from Animation import Animation

class AnimationDirector:
    def __init__(self, animations):
        # Animation 객체들을 복사
        self.anis = [i for i in animations]
        self.index = 0
    
    def changeAnimation(self, index):
        int_index = int(index)
        if int_index >= 0 and int_index < len(self.anis):
            self.index = int_index
            self.anis[self.index].index = 0

    def update(self, sprite):
        nextIndex = self.anis[self.index].update(sprite)
        if nextIndex != -1:
            self.changeAnimation(nextIndex)