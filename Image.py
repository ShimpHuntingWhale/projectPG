
import pygame

class Image:
    def __init__(self, filepath):
        if type(filepath) == str:
            self.img = pygame.image.load(filepath)
        elif type(filepath) == pygame.Surface:
            self.img = filepath
        self.isFlip = [False, False]
        self.opacity = 1.0
        self.originalRect = self.img.get_rect()
        self.rect = self.img.get_rect()
        self.scale = (1, 1)
    
    def init(self):
        self.setFlip(False, False)
        self.setOpacity(1.0)
        self.setScale(1, 1)

    def setFlip(self, xbool, ybool):
        self.setFlipX(xbool)
        self.setFlipY(ybool)
    
    def setFlipX(self, xbool):
        if self.isFlip[0] != xbool:
            self.img = pygame.transform.flip(self.img, True, False)
        self.isFlip = [xbool, self.isFlip[1]]

    def setFlipY(self, ybool):
        if self.isFlip[1] != ybool:
            self.img = pygame.transform.flip(self.img, False, True)
        self.isFlip = [self.isFlip[0], ybool]
        
    def setOpacity(self, opacity):
        self.opacity = opacity if opacity >= 0.0 else 0.0
        self.opacity = opacity if opacity <= 1.0 else 1.0

    # Scale 관련 함수들
    def setScale(self, xScale, yScale):
        self.scale = (xScale, yScale)
        self.img = pygame.transform.smoothscale(self.img, (self.originalRect.right * xScale, self.originalRect.bottom * yScale))
        self.rect = self.img.get_rect()